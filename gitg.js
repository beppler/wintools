var shell = WScript.CreateObject('WScript.Shell');
var environment = shell.Environment('Process');
var programFiles = environment('ProgramFiles');

var program = '"' + programFiles + '\\gitg\\bin\\gitg.exe"';

var args = WScript.Arguments;
var params = '';
for (var i = 0; i < args.length; i++)
    params += ' "' + args(i).replace('"', '""') + '"';

shell.Run(program + params);
