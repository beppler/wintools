// "%SystemRoot%\Microsoft.NET\Framework\v4.0.30319\csc.exe" elevate.cs
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

class Program
{
  static int Main(string[] args)
  {
    if (args.Length == 0)
    {
      Console.WriteLine("Usage: {0}.exe command [params]", Assembly.GetExecutingAssembly().GetName().Name);
      return 2;
    }

    try {
      string @params = string.Join(" ", args.Skip(1).Select(param => param.Contains(" ") || param.Contains("\"") ? string.Format("\"{0}\"", param.Replace("\"", "\"\"")) : param));

      ProcessStartInfo psi = new ProcessStartInfo(args[0], @params);
      psi.Verb = "runas";
      psi.WorkingDirectory = Environment.CurrentDirectory;
      Process.Start(psi);

      return 0;
    }
    catch (Exception ex) {
      Console.Error.WriteLine("Error: {0}", ex.Message);
      return 1;
    }
  }
}
