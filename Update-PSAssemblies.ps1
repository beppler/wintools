$env:Path = ([Runtime.InteropServices.RuntimeEnvironment]::GetRuntimeDirectory(), $env:Path) -join [System.IO.Path]::PathSeparator
[AppDomain]::CurrentDomain.GetAssemblies() | % {
  if (! $_.location) {continue}
  $Name = Split-Path $_.location -leaf
  Write-Host -ForegroundColor Yellow "NGENing : $Name"
  ngen install $_.location | % {"`t$_"}
}