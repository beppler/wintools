var shell = new ActiveXObject('WScript.Shell');
var environment = shell.Environment('Process');
var fs = new ActiveXObject('Scripting.FileSystemObject');

var programs = environment('ProgramFiles');
if (!fs.FolderExists(programs)) {
    programs = environment('ProgramFiles(x86)');
}

var folder = programs + '\\Microsoft VS Code Insiders';
var code = folder + 'Code - Insiders.exe';
if (!fs.FolderExists(folder)) {
    folder = programs + '\\Microsoft VS Code';
    code = 'Code.exe';
}

environment("VSCODE_DEV") = "";
environment("ELECTRON_RUN_AS_NODE") = "1";

var args = WScript.Arguments;
var params = '';
for (var i = 0; i < args.length; i++)
    params += ' "' + args(i).replace('"', '""') + '"';

var command = '"' + folder + "\\" + code + '"' + ' "' + folder + '\\resources\\app\\out\\cli.js"' + params;

shell.Run(command);
