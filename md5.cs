using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace md5 {
    class Program {
        public static int Main(string[] args) {
            try {
                if (args.Length < 1) {
                    Console.Error.WriteLine("Usage: md5 file1 [file2 ...]");
                    return 2;
                }

                foreach (var name in args) {
                    byte[] hash;
                    using (FileStream stream = File.Open(name, FileMode.Open)) {
                        MD5 hasher = MD5.Create();
                        hash = hasher.ComputeHash(stream);
                    }
                    StringBuilder builder = new StringBuilder(hash.Length * 2);
                    for (int i = 0; i < hash.Length; i++)
                      builder.Append(hash[i].ToString("x2"));
                    Console.WriteLine("{0}  {1}", builder, name);
                }
            }
            catch (Exception exc) {
                Console.Error.WriteLine("Error: {0}",exc.Message);
                return 1;
            }
            return 0;
        }
    }
}