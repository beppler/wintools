Param (
    [Parameter(Position=1)][string]$HostName = [System.Net.Dns]::GetHostEntry("localhost").HostName, # get full qualified host name
    [Switch]$WhatIf = $false
)

# grab the thumbprint of the last expiring SSL cert in the computer store for host
$cert = Get-ChildItem -Path Cert:\LocalMachine\My -DnsName $hostname | Sort-Object NotAfter -Descending | Select-Object -First 1

If (-not $cert) {
    Write-Output ("Certificate not found to: {0}" -f $hostname)
}
Else {
    Write-Output "Configuring Terminal Services Certificate with"
    Format-List -InputObject $cert -Property Subject,Issuer,Thumbprint,NotBefore,NotAfter

    # get a reference to the config instance (https://msdn.microsoft.com/en-us/library/aa383799(v=vs.85).aspx)
    $tsgs = Get-WmiObject -Class Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -Filter "TerminalName='RDP-tcp'"

    If (-not $WhatIf) {
        # set the new thumbprint value
        $tsgs.SSLCertificateSHA1Hash = $cert.Thumbprint
        $tsgs.SSLCertificateSHA1HashType = 3 # Custom
        $tsgs.Put()
    }
}
