var shell = WScript.CreateObject('WScript.Shell');
var environment = shell.Environment('Process');
var programs = environment('ProgramW6432');

var sublimeText = '"' + programs + '\\Sublime Text 3\\sublime_text.exe"';

var args = WScript.Arguments;
var params = '';
for (var i = 0; i < args.length; i++)
    params += ' "' + args(i).replace('"', '""') + '"';

shell.Run(sublimeText + params);
